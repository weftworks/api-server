import Connection from './connection';

import { Table as UserTable } from '../schema/user/user.table';

const User = Connection.define('user', UserTable);

const Options = {
  force: true
};

Connection.sync(Options);
