
import Sequelize from 'sequelize';
import Connection from './connection';

import { Fields as UserFields } from '../schema/user/user.table';

const Database = Connection;

const User = Database.define('user', UserFields);

export default Database;
