
import Sequelize from 'sequelize';
import _ from 'lodash';

import Config from '../config/config';

const Connection = new Sequelize(
  Config.db.schema, Config.db.user, Config.db.password,
  {
    dialect: Config.db.dialect,
    host: Config.db.host
  }
);

export default Connection;
