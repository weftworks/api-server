import Express from 'express';
import GraphHTTP from 'express-graphql';
import Jwt from 'express-jwt';

import Config from './config/config';
import Schema from './schema/schema';

var app = Express();

app.use('/api',
  Jwt({
    secret: Config.jwt.secret,
    requestProperty: 'auth',
    credentialsRequired: false
  }),
  GraphHTTP({
    schema: Schema,
    graphiql: false
  })
);

app.use('/graphql',
  GraphHTTP({
    schema: Schema,
    graphiql: true
  })
);

app.listen(Config.api.port, () => console.log('Weftworks API is now listening at port %d.', Config.api.port));
