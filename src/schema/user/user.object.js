
import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull
} from 'graphql';

const User = new GraphQLObjectType({
  name: 'User',
  description: 'User object',
  fields: () => {
    return {
      id: {
        type: GraphQLString
      },
      email: {
        type: GraphQLString
      },
      password: {
        type: GraphQLString
      }
    };
  }
});

export default User;
