
import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull
} from 'graphql';

import Database from '../../../database/database';
import User from '../user.object';

const Query = {
  type: new GraphQLList(User),
  args: {
    id: {
      type: GraphQLString
    },
    email: {
      type: GraphQLString
    }
  },
  resolve (root, args) {
    const where = {};
    if (args.id != null) {
      where.id = args.id;
    }
    if (args.email != null) {
      where.email = args.email;
    }
    return Database.models.user.findAll({ where: where });
  }
};

export default Query;
