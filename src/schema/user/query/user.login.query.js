import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull
} from 'graphql';

import bcrypt from 'bcrypt-nodejs';
import Jwt from 'jsonwebtoken';

import Config from '../../../config/config';
import Database from '../../../database/database';
import User from '../user.object';

const Query = {
  type: GraphQLString,
  args: {
    email: {
      type: GraphQLString
    },
    password: {
      type: GraphQLString
    }
  },
  resolve (root, args) {
    if (args.email != null && args.password != null) {
      const where = {};
      where.email = args.email;
      return Database.models.user.findOne({ where: where })
        .then(function(user) {
          if (user != null) {
            if (bcrypt.compareSync(args.password, user.password)) {
              var token = Jwt.sign({
                roles: 1
              },
              Config.jwt.secret,
              {
                algorithm: Config.jwt.algorithm,
                subject: user.email,
                expiresIn: Config.jwt.expiration
              });
              return token;
            }
          }
      });
    }
    return null;
  }
};

export default Query;
