
import GetQuery from './query/user.get.query';
import LoginQuery from './query/user.login.query';

const Query = {
  users: GetQuery,
  login: LoginQuery
};

export default Query;
