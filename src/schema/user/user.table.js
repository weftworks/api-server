
import Sequelize from 'sequelize';

const Fields = {
  id: {
    type: Sequelize.UUID,
    primaryKey: true,
    validate: {
      isUUID: 4
    }
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true
    }
  },
  password: {
    type: Sequelize.STRING(60),
    allowNull: false
  }
};

const Table = ('user', Fields);

export { Table, Fields };
