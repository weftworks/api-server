
import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull
} from 'graphql';
import bcrypt from 'bcrypt-nodejs';
import uuidv4 from 'uuid';

import Database from '../../../database/database';
import User from '../user.object';

const Mutation = {
  type: User,
  args: {
    email: {
      type: new GraphQLNonNull(GraphQLString)
    },
    password: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  resolve (root, args) {
    return Database.models.user.create({
      id: uuidv4(),
      email: args.email.toLowerCase(),
      password: bcrypt.hashSync(args.password, bcrypt.genSaltSync(8), null)
    });
  }
};

export default Mutation;
