
import CreateMutation from './mutation/user.create.mutation';

const Mutation = {
  createUser: CreateMutation
};

export default Mutation;
