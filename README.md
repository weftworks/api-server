# Weftworks API Server

[![pipeline status](https://gitlab.com/weftworks/api-server/badges/staging/pipeline.svg)](https://gitlab.com/weftworks/api-server/commits/staging)
[![coverage report](https://gitlab.com/weftworks/api-server/badges/staging/coverage.svg)](https://gitlab.com/weftworks/api-server/commits/staging)

## Description

API server for the Weftworks project.
